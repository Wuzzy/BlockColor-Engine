--Minetest
--Copyright (C) 2013 sapier
--
--This program is free software; you can redistribute it and/or modify
--it under the terms of the GNU Lesser General Public License as published by
--the Free Software Foundation; either version 2.1 of the License, or
--(at your option) any later version.
--
--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU Lesser General Public License for more details.
--
--You should have received a copy of the GNU Lesser General Public License along
--with this program; if not, write to the Free Software Foundation, Inc.,
--51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

--------------------------------------------------------------------------------

local engine = {
	"BlockColor use Minetest (Celeron55) / Multicraft (Monte48) ",
}

local developers = {
	"Mrchiantos",
}

local contributors = {

"Animal Model : AspireMint",
"Ships Spawn Mod : SokoMine",
"Furnitures Mod : Gerold55",
"Slope Simple : Nigel",
"Awards : RubenWardy",
"Mainmenu : RubenWardy (I have use Pr 5.0 Modify for 0.4)",
"SurfBoard : Archfan7411",
"Airboat : Paramat",
"DriftCar : Paramat",
"Spaceship : Paramat (Code) & SpaceShip'Model (Viktor Hahn )",
"Textures 16px : Peak (Since 1.46.4b)",
"Trampoline : hkzorman",
"Mobs mod : TenPlus1",
"Player Model : Quaternius (Human Low Poly / Since 1.53)",
"Player Model b3d : Kroukuk",
"Trees : Kenney.nl (Since 1.53)",
"Hdb Model (Homedecor) : Vanessa",
"Hovercraft : Stuart Jones ",
"HotAirBallons : Me Me and Me // mbb (Flying Carpet - Wuzzy)",
"ComboBlock : Pithydon",
"ComboBlock & ComboStair Model : Nathan (MinetestVideo)",
"Header and Logo : LMD, azekill_DIABLO & MrChiantos",
"Inventory Icons : Xenon (xenonca)",

}


local function buildCreditList(source)
	local ret = {}
	for i = 1, #source do
		ret[i] = core.formspec_escape(source[i])
	end
	return table.concat(ret, ",,")
end

return {
	name = "credits",
	caption = fgettext("Credits"),
	cbf_formspec = function(tabview, name, tabdata)
		local logofile = defaulttexturedir .. "logo.png"
		local version = core.get_version()
		return "image[0.25,1;" .. core.formspec_escape(logofile) .. "]" ..
			"tablecolumns[color;text]" ..
			"tableoptions[background=#00000000;highlight=#00000000;border=false]" ..
			"table[0,0;11.75,5.55;list_credits;" ..
			"#FFFF00," .. fgettext("Engine") .. ",," ..
			buildCreditList(engine) .. "," ..
			",," ..
			"#FFFF00," .. fgettext("Developers") .. ",," ..
			buildCreditList(developers) .. "," ..
			",," ..
			"#FFFF00," .. fgettext("Contributors") .. ",," ..
			buildCreditList(contributors) .. "," 
			..";1]"
	end
}
