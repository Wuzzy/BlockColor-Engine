Blockcolor Open Source Project
==============================

Blockcolor is an Open Source sandbox creative game with 8 Colors. (https://blockcolor.net/).

Blockcolor is based on Multicraft/Minetest projects - Copyright © 2018-2019 - MrChiantos & BlockColor Community.

GNU LGPL 3.0 for code and Cc By SA 3.0 for medias (http://creativecommons.org/licenses/by-sa/3.0/)

---

__Game Creator__ : MrChiantos 

Helpers or others Creators :

__Animal Model :__ AspireMint  
__Ships Spawn Mod :__ SokoMine  
__Furnitures Mod :__ Gerold55  
__Slope Simple :__ Nigel  
__Awards :__ Ruben  
__SurfBoard :__ Archfan7411  
__Airboat :__ Paramat  
__DriftCar :__ Paramat  
__Spaceship :__ Paramat (Code) & SpaceShip'Model (Viktor Hahn )  
__Textures 16px :__ Peak (Since 1.46.4b)  
__Trampoline :__ hkzorman  
__Mobs mod :__ TenPlus1  
__Player Model :__ Quaternius (Human Low Poly / Since 1.53)  
__Player Model b3d :__ Kroukuk  
__Trees :__ Kenney.nl (Since 1.53)  
__Hdb Model (Homedecor) :__ Vanessa  
__Hovercraft :__ Stuart Jones  
__HotAirBallons :__ Me Me and Me // mbb (Flying Carpet - Wuzzy)  
__ComboBlock :__ Pithydon  
__ComboBlock & ComboStair Model :__ Nathan (MinetestVideo)  
__Header and Logo :__ LMD, azekill_DIABLO & MrChiantos  
__Inventory Icons :__ Xenon (xenonca)  

Minetest   : Copyright © 2010-2019 celeron55, Perttu Ahola <celeron55@gmail.com> & Minetest Community and Devs.  

Multicraft : Copyright © 2014-2019 Maksim Gamarnik [MoNTE48] MoNTE48@mail.ua & MultiCraft Development Team.

---

If I forgot you, thank you to contact me via the forum of minetest, I could add you to the list of names.
